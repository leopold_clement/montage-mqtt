import paho.mqtt.client as mqtt
import logging
import time

logging.basicConfig(level=logging.INFO)

nom = "CHANGE DE NOM"
host = "montage-mqtt.kokarde.fr"


def callbackConnection(client: mqtt.Client, userdata, flags, rc):
    client.subscribe("#")


def callbackMessage(client: mqtt.Client, userdata, msg: mqtt.MQTTMessage):
    logging.info("{} : {}".format(msg.topic, msg.payload))


client = mqtt.Client(client_id="pahoPy_{}_script".format(
    nom), clean_session=True, transport="websockets")

client.on_connect = callbackConnection
client.on_message = callbackMessage
client.will_set("python", payload="coucou")

client.ws_set_options(path="/mosquitto")
client.connect(host, port=80, keepalive=60)


client.loop_start()
logging.debug("Début de la boucle")

time.sleep(60)

logging.debug("Fin de la boucle")
client.loop_stop()
