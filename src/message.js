class MessageChat {
  constructor(emeteur, text) {
    this.emeteur = emeteur;
    this.contenu = text;
    this.hash = this.emeteur + this.contenu;
  }
  string() {
    return `{ "emeteur" : "${this.emeteur}" , "contenu" : "${this.contenu}" }`;
  }
}

export default MessageChat;
